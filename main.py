from typing import Optional
from fastapi import FastAPI, File, UploadFile
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import shutil

class Student(BaseModel):
    npm: str
    name: str
    batch: int


class UpdateStudentRequest(BaseModel):
    name: Optional[str]
    batch: Optional[int]


student_list = [
    {
        "npm": "1806133774",
        "name": "Glenda Emanuella Sutanto",
        "batch": 2018
    }
]


app = FastAPI()


def find_student_by_npm(npm: str):
    for i in range(len(student_list)):
        print(student_list[i])
        if student_list[i]["npm"] == npm:
            return student_list[i]

    return None


@app.get("/students")
async def get_all_student():
    return JSONResponse(
        status_code = 200,
        content = student_list
    )


@app.get("/student/{npm}")
async def get_student_by_npm(npm: str):
    student = find_student_by_npm(npm)
    if student:
        return JSONResponse(
            status_code = 200,
            content = student
        )

    return JSONResponse(
        status_code = 400,
        content = {
            "error": "invalid_request",
            "error_description": f"There's no student with npm {npm}."
        }
    )


@app.post("/student")
async def add_student(student: Student):
    try:
        student_dict = student.dict()
        student_list.append(student_dict)
        return JSONResponse(
            status_code = 200,
            content = {
                "message": "Student has been successfully added.",
                "data": student_dict
            }
        )
    except:
        return JSONResponse(
            status_code = 400,
            content = {
                "error": "invalid_request",
                "error_description": "There's an error in your request."
            }
        )


@app.put("/student/{npm}")
async def update_student(npm: str, update_student_request: UpdateStudentRequest):
    try:
        student = find_student_by_npm(npm)
        if update_student_request.name:
            student["name"] = update_student_request.name
        if update_student_request.batch:
            student["batch"] = update_student_request.batch
        return JSONResponse(
            status_code = 200,
            content = {
                "message": f"Student data with npm {npm} has been successfully updated.",
                "data": student
            }
        )
    except:
        return JSONResponse(
            status_code = 400,
            content = {
                "error": "invalid_request",
                "error_description": "There's an error in your request."
            }
        )


@app.delete("/student/{npm}")
async def delete_student(npm: str):
    try:
        student = find_student_by_npm(npm)
        student_list.remove(student)
        return JSONResponse(
            status_code = 200,
            content = {
                "message": f"Student with npm {npm} has been successfully deleted.",
            }
        )
    except:
        return JSONResponse(
            status_code = 400,
            content = {
                "error": "invalid_request",
                "error_description": "There's an error in your request."
            }
        )


@app.post("/uploadfile")
async def upload_file(file: UploadFile = File(...)):
    try:
        with open(f"FastAPI {file.filename}", "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        
        return JSONResponse(
            status_code = 200,
            content = {
                "message": f"File {file.filename} has been successfully uploaded."
            }
        )
    except:
        return JSONResponse(
            status_code = 400,
            content = {
                "error": "invalid_request",
                "error_description": "There's an error in your request."
            }
        )
